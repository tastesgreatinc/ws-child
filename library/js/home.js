jQuery(function($){
	/*
	 * On the Home page, make the Message widget title
	 * a link to the About page.
	 */
	var widgetTitle = $("#messagearea .widget-title");
	var aboutLink = $(document.createElement('a'));
	aboutLink.attr('href', '/about').attr('id', 'pledge-link');
	widgetTitle.replaceWith(
			  $(aboutLink).append(widgetTitle.clone())
	);
});