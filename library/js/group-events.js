jQuery(function($){
	$('#tripleseat_embed_form h2').each(function(index, element){
		var elem = $(element);
		var text = elem.text();
		var textArray = text.split(' ');
		if(textArray[0].match(/your/i)){
			textArray.shift();
		}
		elem.text(textArray.join(' '));
	});
});