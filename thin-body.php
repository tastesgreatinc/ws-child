<?php
/*

Template Name:	Thin Template

*/
?>
<style>
.wrapper{
  width: 55%;
}
.page-title-wrap {
    display: none;
}
#main-nav li {
    padding: 2% 2% 1% 4% !important;
}
</style>
<?php get_header(); ?>

<?php
	/**
	 * travelify_before_main_container hook
	 */
	do_action( 'travelify_before_main_container' );
?>

<div id="container">
	<?php
		/**
		 * travelify_main_container hook
		 *
		 * HOOKED_FUNCTION_NAME PRIORITY
		 *
		 * travelify_content 10
		 */
		do_action( 'travelify_main_container' );
	?>
</div><!-- #container -->

<?php
	/**
	 * travelify_after_main_container hook
	 */
	do_action( 'travelify_after_main_container' );
?>
</br>
<?php get_footer(); ?>
