<?php
/**
 * Displays the footer section of the theme.
 *
 */
?>
	   </div><!-- #main -->
</br>
	   <?php
	      /**
	       * travelify_after_main hook
	       */
	      do_action( 'travelify_after_main' );
	   ?>

	   <?php
	   	/**
	   	 * travelify_before_footer hook
	   	 */
	   	do_action( 'travelify_before_footer' );
	  
	   	/*
	   	 * Show the Message widget only for the Home and About pages
	   	 */
	   	if(is_page(2) || is_page(10)){
	   		
	  ?> 	
		<div id="messagearea" class="container widget-container content-container clearfix">
			<?php  do_action( 'travelify_message' ); ?>
		</div>
	  <?php 
		}
		
		if( is_page( 744 ) ){
	
		?>
			<div id="press-widget-area" class="container widget-container content-container clearfix">
				<?php  do_action( 'travelify_press' ); ?>
			</div>
		  <?php 
			}
	  ?>
	   <footer id="footerarea" class="clearfix widget-container content-container">
			<?php
		      /**
		       * travelify_footer hook
				 *
				 * HOOKED_FUNCTION_NAME PRIORITY
				 *
				 * travelify_footer_widget_area 10
				 * travelify_open_sitegenerator_div 20
				 * travelify_socialnetworks 25
				 * travelify_footer_info 30
				 * travelify_close_sitegenerator_div 35
				 * travelify_backtotop_html 40
		       */
		      do_action( 'travelify_footer' );
		   ?>
		</footer>

		<?php
	   	/**
	   	 * travelify_after_footer hook
	   	 */
	   	do_action( 'travelify_after_footer' );
	   ?>

	</div><!-- .wrapper -->

	<?php
		/**
		 * travelify_after hook
		 */
		do_action( 'travelify_after' );
	?>

<?php wp_footer(); ?>
<div class="stamp">
<img src="/wp-content/uploads/2015/08/stamp.png"></div>
</body>
</html>
