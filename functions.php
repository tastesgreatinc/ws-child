<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}

function theme_enqueue_scripts(){
	if( is_page( 2 ) ){
		wp_enqueue_script( 'home', get_stylesheet_directory_uri() . '/library/js/home.js' );		
	}
	
	if( is_page( 10 ) || is_page( 744 ) || is_page( 727 ) ){ // About, Press/Media, and Group Events
		wp_enqueue_script( 'extra-heading', get_stylesheet_directory_uri() . '/library/js/extra-heading.js' );
	}
	
	if( is_page( 727 ) ){ // Group Events
		wp_enqueue_script( 'group-events', get_stylesheet_directory_uri() . '/library/js/group-events.js' );
	}
}

// Start travelify/functions.php override

add_filter('widget_text', 'do_shortcode');
add_filter('widget_text','execute_php',100);

/*
 * Allow all text widgets to be interpreted as PHP if prefixed
 * with the script start tag
 * 
 * http://www.emanueleferonato.com/2011/04/11/executing-php-inside-a-wordpress-widget-without-any-plugin/
 */
function execute_php($html){
	if(strpos($html,"<"."?php")!==false){
		ob_start();
		eval("?".">".$html);
		$html=ob_get_contents();
		ob_end_clean();
	}
	return $html;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
add_action( 'after_setup_theme', 'travelify_setup' );

/**
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 */

function travelify_setup() {
	/**
	 * travelify_add_files hook
	 *
	 * Adding other addtional files if needed.
	 */
	do_action( 'travelify_add_files' );

	/* Travelify is now available for translation. */
	require( get_template_directory() . '/library/functions/i18n.php' );

	/** Load functions - from Child Theme */
	require( get_stylesheet_directory() . '/library/functions/functions.php' );

	/** Load WP backend related functions */
	require( get_template_directory() . '/library/panel/themeoptions-defaults.php' );
	require( get_stylesheet_directory() . '/library/panel/theme-options.php' );
	require( get_template_directory() . '/library/panel/metaboxes.php' );
	require( get_template_directory() . '/library/panel/show-post-id.php' );

	/** Load Shortcodes */
	require( get_template_directory() . '/library/functions/shortcodes.php' );

	/** Load WP Customizer */
	require( get_stylesheet_directory() . '/library/functions/customizer.php' );

	/** Load Structure */
	require( get_stylesheet_directory() . '/library/structure/header-extensions.php' );
	require( get_template_directory() . '/library/structure/sidebar-extensions.php' );
	require( get_template_directory() . '/library/structure/footer-extensions.php' );
	require( get_template_directory() . '/library/structure/content-extensions.php' );

	/**
	 * travelify_add_functionality hook
	 *
	 * Adding other addtional functionality if needed.
	 */
	do_action( 'travelify_add_functionality' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page.
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in header menu location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'travelify' ) );

	// Add Travelify custom image sizes
	add_image_size( 'featured', 670, 300, true );
	add_image_size( 'featured-medium', 230, 230, true );
	add_image_size( 'slider', 1018, 460, true ); 		// used on Featured Slider on Homepage Header
	add_image_size( 'gallery', 474, 342, true ); 				// used to show gallery all images

	// This feature enables WooCommerce support for a theme.
	add_theme_support( 'woocommerce' );

	/**
	 * This theme supports custom background color and image
	 */
	$args = array(
			'default-color' => '#d3d3d3',
			'default-image' => get_template_directory_uri() . '/images/background.png',
	);
	add_theme_support( 'custom-background', $args );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/**
	 * This theme supports add_editor_style
	 */
	add_editor_style();
}
// End travelify/functions.php override

add_action( 'travelify_message', 'travelify_message_area', 1 );
add_action( 'travelify_press', 'travelify_press_area', 1 );
/**
 * Displays the footer widgets
 */
function travelify_message_area() {
	get_sidebar( 'message' );
}
function travelify_press_area() {
	get_sidebar( 'press' );
}

add_action( 'travelify_message_widget', 'travelify_display_message_widget', 10 );
add_action( 'travelify_press_widget', 'travelify_display_press_widget', 10 );
/**
 * Show widgets on Footer of the theme.
 *
 * Shows all the widgets that are dragged and dropped on the Footer Sidebar.
 */
function travelify_display_message_widget() {
	if( is_active_sidebar( 'travelify_message_widget' ) ) {
		?>
		<div class="widget-wrap">
			<div class="container">
				<div class="widget-area clearfix">
				<?php
					// Calling the footer sidebar if it exists.
					if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar( 'travelify_message_widget' ) ):
					endif;
				?>
				</div><!-- .widget-area -->
			</div><!-- .container -->
		</div><!-- .widget-wrap -->
		<?php
	}
}

function travelify_display_press_widget() {
	if( is_active_sidebar( 'travelify_press_widget' ) ) {
		?>
		<div class="widget-wrap">
			<div class="container">
				<div class="widget-area clearfix">
				<?php
					// Calling the footer sidebar if it exists.
					if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar( 'travelify_press_widget' ) ):
					endif;
				?>
				</div><!-- .widget-area -->
			</div><!-- .container -->
		</div><!-- .widget-wrap -->
		<?php
	}
}

remove_action( 'travelify_footer', 'travelify_footer_info', 30);