<?php
/**
 * Displays the footer sidebar of the theme.
 *
 */
?>

<?php
	/**
	 * travelify_before_press_widget
	 */
	do_action( 'travelify_before_press_widget' );
?>

<?php
	/**
	 * travelify_press_widget hook
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * travelify_display_press_widget 10
	 */
	do_action( 'travelify_press_widget' );
?>

<?php
	/**
	 * travelify_after_press_widget
	 */
	do_action( 'travelify_after_press_widget' );
?>